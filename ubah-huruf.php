<?php
 function ubah_huruf($string){
//kode di sini
    $abjad = array (range('a','z'));
    $huruf = "";
    foreach (str_split($string) as $str){
         foreach ($abjad as $a){
         for($i=0;$i<count($a);$i++){
             if($str == $a[$i]){
                 $huruf .= $a[$i+1];
             }
         } } 
     }
     return $huruf."<br>";
 }

 // TEST CASES
  echo ubah_huruf('wow'); // xpx
  echo ubah_huruf('developer'); // efwfmpqfs
  echo ubah_huruf('laravel'); // mbsbwfm
  echo ubah_huruf('keren'); // lfsfo
  echo ubah_huruf('semangat'); // tfnbohbu

?>